import 'reflect-metadata';
import application = require("application");
import platform = require("platform");

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";
import { EventData, Observable } from "data/observable";
import { Page } from "ui/page";

import { ObservableArray } from "data/observable-array";
import * as appSettings from "application-settings";

import * as Phone from "nativescript-phone";
import { Color } from "color";
import { ValueList, TelcoTransfer } from "../../shared/key-value-object";
import { AndroidUtil } from "../../shared/app-util";

let frameModule = require("ui/frame");

let dialogs = require("ui/dialogs");
var contacts = require("nativescript-contacts");
var myself: TransferComponent;

declare var UIBarStyle: any;

let androidUril: AndroidUtil = new AndroidUtil();

@Component({
    selector: "transfer",
    templateUrl: "./page/transfer/view.html",
    styleUrls: ["./page/transfer/style.css"]
})
export class TransferComponent implements OnInit {

    public networkName: TelcoTransfer;
    public providerSelectedIndex: number;
    public phoneNumber: string;
    public amount: number;
    public myPassword: string;

    private _sub: any;

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private page: Page) {
        myself = this;
        if (this.page.ios) {
            var navigationBar = frameModule.topmost().ios.controller.navigationBar;
            navigationBar.barStyle = UIBarStyle.blackTranslucent;
        }

        this.networkName = new TelcoTransfer([
            { Id: "", Name: "Select a network", TransferTemplate: "", DefaultPassword: "", ChangePasswordTemplate: "" },
            { Id: "mtn", Name: "MTN", TransferTemplate: "*600*[NUMBER]*[AMOUNT]*[PASSWORD]#", DefaultPassword: "0000", ChangePasswordTemplate: "" },
            { Id: "glo", Name: "Globacom", TransferTemplate: "*131*[NUMBER]*[AMOUNT]*[PASSWORD]#", DefaultPassword: "00000", ChangePasswordTemplate: "" },
            { Id: "etisalat", Name: "Etisalat", TransferTemplate: "*223*[PASSWORD]*[AMOUNT]*[NUMBER]#", DefaultPassword: "0000", ChangePasswordTemplate: "" },
            { Id: "airtel", Name: "Airtel", TransferTemplate: "2U [NUMBER] [AMOUNT] [PASSWORD]", DefaultPassword: "1234", ChangePasswordTemplate: "" },
        ]);

        this.providerSelectedIndex = 0;
        this.phoneNumber = "";
    }

    ngOnInit() {

        this.providerSelectedIndex = this.networkName.getIndex(appSettings.getString("telco"));
        this.myPassword = this.networkName.getItemObjectAt(this.providerSelectedIndex).DefaultPassword;

        this._sub = this.route.params.subscribe(params => {
            if (this.page.ios) {
                var navigationBar = frameModule.topmost().ios.controller.navigationBar;
                navigationBar.translucent = true;
                navigationBar.barStyle = UIBarStyle.blackTranslucent;
            }
            else {
                this.page.actionBarHidden = true;
            }
        });

        this.amount = 100;

    }

    ngOnDestroy() {
        this._sub.unsubscribe();
    }

    public onProviderChange(selectedi) {
        this.providerSelectedIndex = selectedi;
        this.myPassword = this.networkName.getItemObjectAt(this.providerSelectedIndex).DefaultPassword;
    }

    public getContact(event: EventData) {
        androidUril.requestPermission(
            androidUril.android.Manifest.permission.READ_CONTACTS,
            AndroidUtil.ANDROID_6_PERMISSION_REQUEST_CODE_READ_CONTACTS
        ).then((granted) => {
            if (granted === -1) {
                alert("You have already revoked READ_CONTACTS permission to this App, Please enable it to proceed");
            }
            else if (granted) {
                contacts.getContact().then((args) => {
                    /// Returns args:: 
                    /// args.data: Generic cross platform JSON object 
                    /// args.reponse: "selected" or "cancelled" depending on wheter the user selected a contact.  

                    if (args.response === "selected") {
                        var contact = args.data; //See data structure below 

                        // lets say you wanted to grab first name and last name 
                        var displayName = contact.name.given + " " + contact.name.family;

                        //lets say you want to get the phone numbers 
                        var phoneNumbers = [];
                        contact.phoneNumbers.forEach(function (phone) {
                            phoneNumbers.push(phone.value);
                        });

                        if (phoneNumbers.length > 1) {
                            dialogs.action({
                                message: displayName,
                                cancelButtonText: "Cancel",
                                actions: phoneNumbers
                            }).then((result) => {
                                console.log("Dialog result: " + result)
                                this.formatAndPlacePhoneNumber(result);
                            });
                        } else if (phoneNumbers.length == 1) {
                            this.formatAndPlacePhoneNumber(phoneNumbers[0]);
                        } else {
                            alert("No phone number found for " + displayName);
                        }

                    }
                });
            }
        }).catch((e) => {
            console.log("Permission Check Error", e);
        });
    }

    formatAndPlacePhoneNumber(phone) {
        phone = new String(phone).valueOf().replace(/[^0-9]/g, '');
        phone = phone.slice(-10);
        if (phone.length < 10) {
            alert("Invalid phone number");
            return false;
        }

        console.log("giiiiii:" + phone)
        this.phoneNumber = "0" + phone;
    }

    private getPhoneNumber() {
        return this.phoneNumber.replace(/[^0-9]/g, '');
    }

    private getAmount() {
        return this.amount.toString().replace(/[^0-9]/g, '');
    }

    private getPassword() {
        return this.myPassword.replace(/[^0-9]/g, '');
    }

    public getUSSDCode() {
        if (this.providerSelectedIndex && this.getAmount() && this.getPhoneNumber() && this.getPassword()) {
            var dialTemp = this.networkName.getItemObjectAt(this.providerSelectedIndex).TransferTemplate
            dialTemp = dialTemp.replace('[NUMBER]', this.getPhoneNumber());
            dialTemp = dialTemp.replace('[AMOUNT]', this.getAmount());
            dialTemp = dialTemp.replace('[PASSWORD]', this.getPassword());

            return dialTemp;
        }

        return "";
    }

    public makeRequest(event: EventData) {
        var telco = this.networkName.getItemObjectAt(this.providerSelectedIndex).Id;
        if (this.getUSSDCode() && this.networkName.getItemObjectAt(this.providerSelectedIndex).Id !== 'airtel') {
            if (application.android) {
                androidUril.requestPermission(
                    androidUril.android.Manifest.permission.CALL_PHONE,
                    AndroidUtil.ANDROID_6_PERMISSION_REQUEST_CODE_CALL_PHONE
                ).then((granted) => {
                    if (granted === -1) {
                        alert("You have already revoked CALL permission to this App, Please enable it to proceed");
                    }
                    else if (granted) {
                        appSettings.setString("telco", telco);
                        Phone.dial(encodeURIComponent(this.getUSSDCode()), false);
                    }
                });
            } else {
                alert("You can not make a USSD call on this device");
            }
        }
        else if (this.getUSSDCode() && this.networkName.getItemObjectAt(this.providerSelectedIndex).Id == 'airtel') {
            appSettings.setString("telco", telco);
            Phone.sms(["432"], this.getUSSDCode());
        }
        else {
            alert("Please fill the form correctly!! ");
        }
    }
}

