import 'reflect-metadata';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EventData, Observable } from "data/observable";
import { Page } from "ui/page";

import { ObservableArray } from "data/observable-array";
import { Location } from "@angular/common";

import * as SocialShare from "nativescript-social-share";
import * as appSettings from "application-settings";
import { Color } from "color";
import { ValueList } from "../../shared/key-value-object";

let frameModule = require("ui/frame");

declare var UIBarStyle: any;

@Component({
    selector: "request",
    templateUrl: "./page/request/view.html",
    styleUrls: ["./page/request/style.css"]
})
export class RequestComponent implements OnInit {

    public phoneNumber: string;
    public networkName: ValueList;
    public providerSelectedIndex: number;
    public amount: number;
    public saveAsTemplate: boolean;

    private _sub: any;

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private page: Page) {

        if (this.page.ios) {
            var navigationBar = frameModule.topmost().ios.controller.navigationBar;
            navigationBar.barStyle = UIBarStyle.blackTranslucent;
        }

        this.networkName = new ValueList([
            { ValueMember: "", DisplayMember: "Select a Network" },
            { ValueMember: "mtn", DisplayMember: "MTN" },
            { ValueMember: "glo", DisplayMember: "Globacom" },
            { ValueMember: "etisalat", DisplayMember: "Etisalat" },
            { ValueMember: "airtel", DisplayMember: "Airtel" }
        ]);

        this.phoneNumber = "";

        this.providerSelectedIndex = 0;
        this.saveAsTemplate = true;
        this.amount = 500;
    }

    ngOnInit() {

        this.phoneNumber = appSettings.getString("myPhoneNumber");
        this.providerSelectedIndex = this.networkName.getIndex(appSettings.getString("telco"));

        this._sub = this.route.params.subscribe(params => {
            if (this.page.ios) {
                var navigationBar = frameModule.topmost().ios.controller.navigationBar;
                navigationBar.translucent = true;
                navigationBar.barStyle = UIBarStyle.blackTranslucent;
            }
            else {
                this.page.actionBarHidden = true;
            }
        });
    }

    ngOnDestroy() {
        this._sub.unsubscribe();
    }

    public onProviderChange(selectedi) {
        this.providerSelectedIndex = selectedi;
    }

    private getPhoneNumber() {
        return this.phoneNumber.replace(/[^0-9]/g, '');
    }

    private getAmount() {
        return this.amount.toString().replace(/[^0-9]/g, '');
    }

    public makeRequest(event: EventData) {
        if (this.saveAsTemplate) {

        }

        if (this.getPhoneNumber() && this.getAmount() && this.providerSelectedIndex > 0) {
            var telco = this.networkName.getItemObjectAt(this.providerSelectedIndex).ValueMember;
            let shareUrl = "http://www.gidiapps.com/oneclick/send/" + telco + "/" + this.getPhoneNumber() + "/" + this.getAmount();

            let message = "Please send me ₦" + this.getAmount() + " airtime to my phone number " + this.getPhoneNumber() +
                " or click on the link below to use OneClick to purchase airtime now\n" + shareUrl;

            appSettings.setString("myPhoneNumber", this.getPhoneNumber());
            appSettings.setString("telco", telco);

            SocialShare.shareText(message);
        } else {
            alert("Please fill the form correctly!! ");
        }
    }
}

