import 'reflect-metadata';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EventData, Observable } from "data/observable";
import { Page } from "ui/page";

import { ObservableArray } from "data/observable-array";
import { Location } from "@angular/common";

import * as SocialShare from "nativescript-social-share";
import * as appSettings from "application-settings";
import { Color } from "color";
import { ValueList } from "../../shared/key-value-object";

let frameModule = require("ui/frame");

declare var UIBarStyle: any;

@Component({
    selector: "request",
    templateUrl: "./page/settings/view.html",
    styleUrls: ["./page/settings/style.css"]
})
export class SettingsComponent implements OnInit {

    public phoneNumber: string;
    public networkName: ValueList;
    public bankItems: ValueList;
    public providerSelectedIndex: number;
    public providerSelectedIndex2: number;
    public amount: number;
    public saveAsTemplate: boolean;

    private _sub: any;

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private page: Page) {

        if (this.page.ios) {
            var navigationBar = frameModule.topmost().ios.controller.navigationBar;
            navigationBar.barStyle = UIBarStyle.blackTranslucent;
        }

        this.networkName = new ValueList([
            { ValueMember: "", DisplayMember: "Select a Network" },
            { ValueMember: "mtn", DisplayMember: "MTN" },
            { ValueMember: "glo", DisplayMember: "Globacom" },
            { ValueMember: "etisalat", DisplayMember: "Etisalat" },
            { ValueMember: "airtel", DisplayMember: "Airtel" }
        ]);

        this.bankItems = new ValueList([
            { ValueMember: "", DisplayMember: "Select a bank" },
            { ValueMember: "901", DisplayMember: "Access Bank" },
            { ValueMember: "937", DisplayMember: "Diamond Bank" },
            { ValueMember: "326", DisplayMember: "EcoBank" },
            { ValueMember: "770", DisplayMember: "Fidelity Bank" },
            { ValueMember: "389*214", DisplayMember: "First City Monument Bank (FCMB)" },
            { ValueMember: "894", DisplayMember: "First Bank" },
            { ValueMember: "737", DisplayMember: "Guaranty Trust Bank (GTB)" },
            { ValueMember: "322*030", DisplayMember: "Heritage Bank" },
            { ValueMember: "322*082", DisplayMember: "Keystone Bank" },
            { ValueMember: "833", DisplayMember: "Skye Bank" },
            { ValueMember: "909", DisplayMember: "Stanbic IBTC Bank" },
            { ValueMember: "389*068", DisplayMember: "Standard Chartered Bank" },
            { ValueMember: "822", DisplayMember: "Sterling Bank" },
            { ValueMember: "389*032", DisplayMember: "Union Bank" },
            { ValueMember: "919", DisplayMember: "United Bank for Africa (UBA)" },
            { ValueMember: "389*215", DisplayMember: "Unity Bank" },
            { ValueMember: "322*035", DisplayMember: "Wema Bank" },
            { ValueMember: "966", DisplayMember: "Zenith Bank" },
        ]);

        this.phoneNumber = "";

        this.providerSelectedIndex = 0;
        this.providerSelectedIndex2 = 0;
        this.saveAsTemplate = true;
        this.amount = 500;
    }

    ngOnInit() {

        this.phoneNumber = appSettings.getString("myPhoneNumber");
        this.providerSelectedIndex = this.networkName.getIndex(appSettings.getString("telco"));
        this.providerSelectedIndex2 = this.bankItems.getIndex(appSettings.getString("bankBuyCode"));

        this._sub = this.route.params.subscribe(params => {
            if (this.page.ios) {
                var navigationBar = frameModule.topmost().ios.controller.navigationBar;
                navigationBar.translucent = true;
                navigationBar.barStyle = UIBarStyle.blackTranslucent;
            }
            else {
                this.page.actionBarHidden = true;
            }
        });
    }

    ngOnDestroy() {
        this._sub.unsubscribe();
    }

    public onProviderChange(selectedi) {
        this.providerSelectedIndex = selectedi;
    }

    public onProviderChange2(selectedi) {
        this.providerSelectedIndex2 = selectedi;
    }

    private getPhoneNumber() {
        return this.phoneNumber.replace(/[^0-9]/g, '');
    }

    public makeRequest(event: EventData) {

        appSettings.setString("myPhoneNumber", this.getPhoneNumber());
        appSettings.setString("telco", this.networkName.getItemObjectAt(this.providerSelectedIndex).ValueMember);
        appSettings.setString("bankBuyCode", this.bankItems.getItemObjectAt(this.providerSelectedIndex2).ValueMember);
        //console.log(appSettings.getString("myPhoneNumber"), appSettings.getString("telco"), appSettings.getString("bankBuyCode"));
        alert("Saved");

    }
}

