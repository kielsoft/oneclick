import 'reflect-metadata';
import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { Router, ActivatedRoute } from '@angular/router';
import {Page} from "ui/page";
import { View } from "ui/core/view";
import {Location} from "@angular/common";

import {Color} from "color";

let frameModule = require("ui/frame");

declare var UIBarStyle: any;
declare var android:any;

@Component({
    selector: "home",
    templateUrl: "./page/home/view.html",
    styleUrls: ["./page/home/style.css"],
})
export class HomeComponent implements OnInit {

    @ViewChild("homeLogoImage") homeLogoImage : ElementRef;

    private _sub: any;
    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private page: Page) {
        
        console.log("FirstComponent.constructor() page: " + page);

    }

    ngOnInit() {

        //let homeLogoImage = <View>this.homeLogoImage.nativeElement;
        //homeLogoImage.android.setBackgroundResource(android.R.drawable.dialog_holo_light_frame)

        if(global.intentViewData && !global.intentViewData.used){
            this.router.navigate(["/send"]);
        }

        this._sub = this.route.params.subscribe(params => {
            this.page.actionBarHidden = true;
            // allows seamless background on home page through statusbar :)
            this.page.backgroundSpanUnderStatusBar = true;
            if (this.page.ios) {
                var navigationBar = frameModule.topmost().ios.controller.navigationBar;
                navigationBar.barStyle = UIBarStyle.blackTranslucent;
            }
        });
    }

    ngOnDestroy() {
        this._sub.unsubscribe();
    }

}