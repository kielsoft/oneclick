import 'reflect-metadata';
import application = require("application");
import platform = require("platform");

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";
import { EventData, Observable } from "data/observable";
import { Page } from "ui/page";

import { ObservableArray } from "data/observable-array";
import * as appSettings from "application-settings";
import * as Phone from "nativescript-phone";

import { Color } from "color";

import { ValueList } from "../../shared/key-value-object";
import { AndroidUtil } from "../../shared/app-util";
let frameModule = require("ui/frame");

declare var UIBarStyle: any;

let androidUril: AndroidUtil = new AndroidUtil();


@Component({
    selector: "request",
    templateUrl: "./page/buy/view.html",
    styleUrls: ["./page/buy/style.css"]
})
export class BuyComponent implements OnInit {
    public bankItems: ValueList;
    public providerSelectedIndex: number;
    public amount: number;
    public saveAsTemplate: boolean;

    private _sub: any;

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private page: Page) {

        this.bankItems = new ValueList([
            { ValueMember: "", DisplayMember: "Select a bank" },
            { ValueMember: "901", DisplayMember: "Access Bank" },
            { ValueMember: "937", DisplayMember: "Diamond Bank" },
            { ValueMember: "326", DisplayMember: "EcoBank" },
            { ValueMember: "770", DisplayMember: "Fidelity Bank" },
            { ValueMember: "389*214", DisplayMember: "First City Monument Bank (FCMB)" },
            { ValueMember: "894", DisplayMember: "First Bank" },
            { ValueMember: "737", DisplayMember: "Guaranty Trust Bank (GTB)" },
            { ValueMember: "322*030", DisplayMember: "Heritage Bank" },
            { ValueMember: "322*082", DisplayMember: "Keystone Bank" },
            { ValueMember: "833", DisplayMember: "Skye Bank" },
            { ValueMember: "909", DisplayMember: "Stanbic IBTC Bank" },
            { ValueMember: "389*068", DisplayMember: "Standard Chartered Bank" },
            { ValueMember: "822", DisplayMember: "Sterling Bank" },
            { ValueMember: "389*032", DisplayMember: "Union Bank" },
            { ValueMember: "919", DisplayMember: "United Bank for Africa (UBA)" },
            { ValueMember: "389*215", DisplayMember: "Unity Bank" },
            { ValueMember: "322*035", DisplayMember: "Wema Bank" },
            { ValueMember: "966", DisplayMember: "Zenith Bank" },
        ]);

        this.providerSelectedIndex = 0;
        this.saveAsTemplate = true;
        this.amount = 500;

    }

    ngOnInit() {
        this._sub = this.route.params.subscribe(params => {
            if (this.page.ios) {

                var navigationBar = frameModule.topmost().ios.controller.navigationBar;
                navigationBar.translucent = true;
                navigationBar.barStyle = UIBarStyle.blackTranslucent;
            }
            else {
                this.page.actionBarHidden = true;
            }
            this.providerSelectedIndex = this.bankItems.getIndex(appSettings.getString("bankBuyCode"));
        });
    }

    ngOnDestroy() {
        this._sub.unsubscribe();
    }

    public onProviderChange(selectedi) {
        this.providerSelectedIndex = selectedi;
    }

    private getAmount() {
        return this.amount.toString().replace(/[^0-9]/g, '');
    }

    public getUSSDCode() {
        let ussdCode = '';
        if (this.getAmount()) {
            ussdCode = '*' + this.bankItems.getItemObjectAt(this.providerSelectedIndex).ValueMember + '*' + this.getAmount() + '#';
        }
        return ussdCode;
    }

    public makeRequest(event: EventData) {
        if (this.saveAsTemplate) {

        }
        if (this.getAmount() && this.providerSelectedIndex > 0) {
            if (application.android) {
                androidUril.requestPermission(
                    androidUril.android.Manifest.permission.CALL_PHONE,
                    AndroidUtil.ANDROID_6_PERMISSION_REQUEST_CODE_CALL_PHONE
                ).then((granted) => {
                    if (granted === -1) {
                        alert("You have already revoked CALL permission to this App, Please enable it to proceed");
                    }
                    else if (granted) {
                        appSettings.setString("bankBuyCode", this.bankItems.getItemObjectAt(this.providerSelectedIndex).ValueMember);
                        Phone.dial(encodeURIComponent(this.getUSSDCode()), false);
                    }
                });
            } else {
                alert("You can not make a USSD call on this device");
            }
        } else {
            alert("Please fill the form correctly!! ");
        }
    }
}

