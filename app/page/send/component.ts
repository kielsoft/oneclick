import 'reflect-metadata';
import app = require("application");
import platform = require("platform");

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EventData, Observable } from "data/observable";
import { Page } from "ui/page";

import { ObservableArray } from "data/observable-array";
import { Location } from "@angular/common";

import * as appSettings from "application-settings";
import * as SocialShare from "nativescript-social-share";
import * as Phone from "nativescript-phone";

import { Color } from "color";

import { ValueList } from "../../shared/key-value-object";
import { AndroidUtil } from "../../shared/app-util";
let frameModule = require("ui/frame");

let dialogs = require("ui/dialogs");
var contacts = require("nativescript-contacts");

declare var UIBarStyle: any;
let androidUril: AndroidUtil = new AndroidUtil();

@Component({
    selector: "request",
    templateUrl: "./page/send/view.html",
    styleUrls: ["./page/send/style.css"]
})
export class SendComponent implements OnInit {

    public phoneNumber: string;
    public bankItems: ValueList;
    public providerSelectedIndex: number;
    public amount: number;
    public saveAsTemplate: boolean;

    private _sub: any;

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private page: Page) {
        if (this.page.ios) {

            var navigationBar = frameModule.topmost().ios.controller.navigationBar;
            navigationBar.translucent = true;
            navigationBar.barStyle = UIBarStyle.blackTranslucent;
        }
        else {
            this.page.actionBarHidden = true;
        }

        this.phoneNumber = "";
        this.bankItems = new ValueList([
            { ValueMember: "", DisplayMember: "Select a Bank" },
            { ValueMember: "901", DisplayMember: "Access Bank" },
            { ValueMember: "937", DisplayMember: "Diamond Bank" },
            { ValueMember: "326", DisplayMember: "EcoBank" },
            { ValueMember: "770", DisplayMember: "Fidelity Bank" },
            { ValueMember: "389*214", DisplayMember: "First City Monument Bank (FCMB)" },
            { ValueMember: "894", DisplayMember: "First Bank" },
            { ValueMember: "737", DisplayMember: "Guaranty Trust Bank (GTB)" },
            { ValueMember: "322*030", DisplayMember: "Heritage Bank" },
            { ValueMember: "322*082", DisplayMember: "Keystone Bank" },
            { ValueMember: "833", DisplayMember: "Skye Bank" },
            { ValueMember: "909", DisplayMember: "Stanbic IBTC Bank" },
            { ValueMember: "389*068", DisplayMember: "Standard Chartered Bank" },
            { ValueMember: "822", DisplayMember: "Sterling Bank" },
            { ValueMember: "389*032", DisplayMember: "Union Bank" },
            { ValueMember: "919", DisplayMember: "United Bank for Africa (UBA)" },
            { ValueMember: "389*215", DisplayMember: "Unity Bank" },
            { ValueMember: "322*035", DisplayMember: "Wema Bank" },
            { ValueMember: "966", DisplayMember: "Zenith Bank" },
        ]);

        this.providerSelectedIndex = 0;
        this.saveAsTemplate = true;
        this.amount = 500;

        if (global.intentViewData && !global.intentViewData.used) {
            this.phoneNumber = (new String(global.intentViewData.phone).valueOf().replace(/[^0-9]/g, ''));
            this.amount = global.intentViewData.amount;
        }
    }

    ngOnInit() {
        this._sub = this.route.params.subscribe(params => {

        });

        this.providerSelectedIndex = this.bankItems.getIndex(appSettings.getString("bankBuyCode"));
    }

    ngOnDestroy() {
        this._sub.unsubscribe();
    }

    public onProviderChange(selectedi) {
        this.providerSelectedIndex = selectedi;
    }

    public getContact(event: EventData) {
        androidUril.requestPermission(
            androidUril.android.Manifest.permission.READ_CONTACTS,
            AndroidUtil.ANDROID_6_PERMISSION_REQUEST_CODE_READ_CONTACTS
        ).then((granted) => {
            if (granted === -1) {
                alert("You have already revoked READ_CONTACTS permission to this App, Please enable it to proceed");
            }
            else if (granted) {
                contacts.getContact().then((args) => {
                    /// Returns args:: 
                    /// args.data: Generic cross platform JSON object 
                    /// args.reponse: "selected" or "cancelled" depending on wheter the user selected a contact.  

                    if (args.response === "selected") {
                        var contact = args.data; //See data structure below 

                        // lets say you wanted to grab first name and last name 
                        var displayName = contact.name.given + " " + contact.name.family;

                        //lets say you want to get the phone numbers 
                        var phoneNumbers = [];
                        contact.phoneNumbers.forEach(function (phone) {
                            phoneNumbers.push(phone.value);
                        });

                        if (phoneNumbers.length > 1) {
                            dialogs.action({
                                message: displayName,
                                cancelButtonText: "Cancel",
                                actions: phoneNumbers
                            }).then((result) => {
                                console.log("Dialog result: " + result)
                                this.formatAndPlacePhoneNumber(result);
                            });
                        } else if (phoneNumbers.length == 1) {
                            this.formatAndPlacePhoneNumber(phoneNumbers[0]);
                        } else {
                            alert("No phone number found for " + displayName);
                        }

                    }
                });
            }
        }).catch((e) => {
            console.log("Permission Check Error", e);
        });
    }

    formatAndPlacePhoneNumber(phone) {
        phone = new String(phone).valueOf().replace(/[^0-9]/g, '');
        phone = phone.slice(-10);
        if (phone.length < 10) {
            alert("Invalid phone number");
            return false;
        }

        this.phoneNumber = "0" + phone;
    }

    private getPhoneNumber() {
        return this.phoneNumber.replace(/[^0-9]/g, '');
    }

    private getAmount() {
        return this.amount.toString().replace(/[^0-9]/g, '');
    }

    public getUSSDCode() {
        let ussdCode = '';
        if (this.getPhoneNumber() && this.getAmount() && this.providerSelectedIndex > 0) {
            ussdCode = '*' + this.bankItems.getItemObjectAt(this.providerSelectedIndex).ValueMember + '*' + this.getAmount() + '*' + this.getPhoneNumber() + '#';
        }
        return ussdCode;
    }

    public makeRequest(event: EventData) {
        if (this.saveAsTemplate) {

        }

        this.phoneNumber;
        if (this.getPhoneNumber() && this.getAmount() && this.providerSelectedIndex > 0) {
            if (app.android) {
                androidUril.requestPermission(
                    androidUril.android.Manifest.permission.CALL_PHONE,
                    AndroidUtil.ANDROID_6_PERMISSION_REQUEST_CODE_CALL_PHONE
                ).then((granted) => {
                    if (granted === -1) {
                        alert("You have already revoked CALL permission to this App, Please enable it to proceed");
                    }
                    else if (granted) {
                        Phone.dial(encodeURIComponent(this.getUSSDCode()), false);
                    }
                }).catch((e) => {
                    console.log("Permission Check Error", e);
                });
            } else {
                alert("You can not make a USSD call on this device");
            }
        } else {
            alert("Please fill the form correctly!! ");
        }
    }
}

