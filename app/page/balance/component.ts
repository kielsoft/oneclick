import 'reflect-metadata';
import application = require("application");
import platform = require("platform");

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";
import { EventData, Observable } from "data/observable";
import { Page } from "ui/page";

import { ObservableArray } from "data/observable-array";
import * as appSettings from "application-settings";

import * as Phone from "nativescript-phone";
import { Color } from "color";
import { ValueList, Telco } from "../../shared/key-value-object";
import { AndroidUtil } from "../../shared/app-util";

let frameModule = require("ui/frame");

declare var UIBarStyle: any;

let androidUril: AndroidUtil = new AndroidUtil();

@Component({
    selector: "balance",
    templateUrl: "./page/balance/view.html",
    styleUrls: ["./page/balance/style.css"]
})
export class BalanceComponent implements OnInit {

    public networkName: Telco;
    public providerSelectedIndex: number;

    private _sub: any;

    constructor(private router: Router, private route: ActivatedRoute, private location: Location, private page: Page) {

        if (this.page.ios) {
            var navigationBar = frameModule.topmost().ios.controller.navigationBar;
            navigationBar.barStyle = UIBarStyle.blackTranslucent;
        }

        this.networkName = new Telco([
            { Id: "", Name: "Select a network", AirtimeBalanceCode: "Select a network", DataBalanceCode: "Select a network" },
            { Id: "mtn", Name: "MTN", AirtimeBalanceCode: "*556#", DataBalanceCode: "*559#" },
            { Id: "glo", Name: "Globacom", AirtimeBalanceCode: "#124*1#", DataBalanceCode: "sms: info to 123" },
            { Id: "etisalat", Name: "Etisalat", AirtimeBalanceCode: "*232#", DataBalanceCode: "*228#" },
            { Id: "airtel", Name: "Airtel", AirtimeBalanceCode: "*123#", DataBalanceCode: "*140#" }
        ]);

        this.providerSelectedIndex = 0;
    }

    ngOnInit() {

        this.providerSelectedIndex = this.networkName.getIndex(appSettings.getString("telco"));

        this._sub = this.route.params.subscribe(params => {
            if (this.page.ios) {
                var navigationBar = frameModule.topmost().ios.controller.navigationBar;
                navigationBar.translucent = true;
                navigationBar.barStyle = UIBarStyle.blackTranslucent;
            }
            else {
                this.page.actionBarHidden = true;
            }
        });
    }

    ngOnDestroy() {
        this._sub.unsubscribe();
    }

    public onProviderChange(selectedi) {
        this.providerSelectedIndex = selectedi;
    }

    public getUSSDCode(type: string) {
        if (type == 'airtime') {
            return this.networkName.getItemObjectAt(this.providerSelectedIndex).AirtimeBalanceCode;
        } else {
            return this.networkName.getItemObjectAt(this.providerSelectedIndex).DataBalanceCode;
        }
    }

    public checkAirtimeBalance(event: EventData) {
        var telco = this.networkName.getItemObjectAt(this.providerSelectedIndex).Id;
        if (this.providerSelectedIndex > 0) {
            if (application.android) {
                androidUril.requestPermission(
                    androidUril.android.Manifest.permission.CALL_PHONE,
                    AndroidUtil.ANDROID_6_PERMISSION_REQUEST_CODE_CALL_PHONE
                ).then((granted) => {
                    if (granted === -1) {
                        alert("You have already revoked CALL permission to this App, Please enable it to proceed");
                    }
                    else if (granted) {
                        appSettings.setString("telco", telco);
                        Phone.dial(encodeURIComponent(this.networkName.getItemObjectAt(this.providerSelectedIndex).AirtimeBalanceCode), false);
                    }
                });
            } else {
                alert("You can not make a USSD call on this device");
            }
        } else {
            alert("Please fill the form correctly!! ");
        }
    }

    public checkDataBalance(event: EventData) {
        var telco = this.networkName.getItemObjectAt(this.providerSelectedIndex).Id;
        if (this.providerSelectedIndex > 0 && this.networkName.getItemObjectAt(this.providerSelectedIndex).Id !== 'glo') {
            if (application.android) {
                androidUril.requestPermission(
                    androidUril.android.Manifest.permission.CALL_PHONE,
                    AndroidUtil.ANDROID_6_PERMISSION_REQUEST_CODE_CALL_PHONE
                ).then((granted) => {
                    if (granted === -1) {
                        alert("You have already revoked CALL permission to this App, Please enable it to proceed");
                    }
                    else if (granted) {
                        appSettings.setString("telco", telco);
                        Phone.dial(encodeURIComponent(this.networkName.getItemObjectAt(this.providerSelectedIndex).DataBalanceCode), false);
                    }
                });
            } else {
                alert("You can not make a USSD call on this device");
            }
        }
        else if (this.providerSelectedIndex > 0 && this.networkName.getItemObjectAt(this.providerSelectedIndex).Id == 'glo') {
            appSettings.setString("telco", telco);
            Phone.sms(["127"], "info");
        }
        else {
            alert("Please fill the form correctly!! ");
        }
    }
}

