import { NativeScriptModule } from 'nativescript-angular/platform';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { NativeScriptHttpModule } from 'nativescript-angular/http';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';

import { TNSFontIconService, TNSFontIconPipe, TNSFontIconPurePipe } from 'nativescript-ng2-fonticon';

import { HomeComponent } from "./page/home/component";
import { RequestComponent } from "./page/request/component";
import { SendComponent } from "./page/send/component";
import { BuyComponent } from "./page/buy/component";
import { BalanceComponent } from "./page/balance/component";
import { TransferComponent } from "./page/transfer/component";
import { SettingsComponent } from "./page/settings/component";
import { FonticonComponent } from "./shared/component/fonticon";

export const routes = [
    { path: "", redirectTo: "/home", pathMatch: "full", terminal: true },
    { path: "home", component: HomeComponent },
    { path: "request", component: RequestComponent },
    { path: "send", component: SendComponent },
    { path: "buy", component: BuyComponent },
    { path: "balance", component: BalanceComponent },
    { path: "transfer", component: TransferComponent },
    { path: "settings", component: SettingsComponent },
];

export const comps = [
    HomeComponent,
    RequestComponent,
    SendComponent,
    BuyComponent,
    BalanceComponent,
    TransferComponent,
    SettingsComponent,
    FonticonComponent,
    TNSFontIconPipe,
    TNSFontIconPurePipe
];

@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptFormsModule,
        NativeScriptHttpModule,
        NativeScriptRouterModule
    ],
    declarations: [
        comps
    ],
    exports: [
        NativeScriptModule,
        NativeScriptFormsModule,
        NativeScriptHttpModule,
        NativeScriptRouterModule,
        comps
    ],
    providers: [{
        provide: TNSFontIconService,
        useFactory: () => {
            return new TNSFontIconService({
                'fa': 'font-awesome.css'
            }, false)
        }
    }]
})
export class ComponentsModule {

}