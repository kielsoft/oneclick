import application = require("application");
import platform = require("platform");

declare var android: any;

if(!!application.android){
    application.android.on(application.AndroidApplication.activityResumedEvent, function (args) { 
        console.log("Event: " + args.eventName + ", Activity: " + args.activity); 
        console.log(new String(args.activity.getIntent().getAction()).valueOf(), new String(android.content.Intent.ACTION_VIEW).valueOf());
        if(new String(args.activity.getIntent().getAction()).valueOf() ==  new String(android.content.Intent.ACTION_VIEW).valueOf()){
            var url:string = new String(args.activity.getIntent().getData()).valueOf();
            try{
                if(url){
                    console.log(url);
                    var requestData = url.split("www.gidiapps.com/oneclick/");
                    if(requestData.length >= 2){
                        requestData = requestData[1].split("/");
                        console.log(requestData.join(';'))

                        if(requestData[0] == 'send'){
                            global.intentViewData = {
                                action: requestData[0],
                                network: requestData[1],
                                phone: requestData[2],
                                amount: requestData[3],
                                used: false
                            };
                        }
                    }
                }
            } catch (e){
                console.log(e);
            }
            
        }
    });
}

export class AndroidUtil {
    public context: any;
    private appActivity: any;
    public android:any;

    public static ANDROID_6_PERMISSION_REQUEST_CODE_CALL_PHONE:number = 111;
    public static ANDROID_6_PERMISSION_REQUEST_CODE_READ_CONTACTS:number = 121;

    constructor() {
        if (application.android) {
            this.android = android;
            this.context = application.android.context;
            this.appActivity = application.android.startActivity;
        }
    }

    public startUSSDCall(phoneNumber: string) {
        console.log("calling your number");
        var intent = new android.content.Intent(android.content.Intent.ACTION_CALL);

        phoneNumber = phoneNumber.replace('#', encodeURIComponent('#'));

        intent.setData(android.net.Uri.parse("tel:" + phoneNumber));
        application.android.foregroundActivity.startActivity(intent);
    }

    public requestPermission(permissionConstant:any, requestCode:number): Promise<any> {
        return new Promise((resolve) => {
            var hasPermission = android.os.Build.VERSION.SDK_INT < 23; // Android M. (6.0)
            console.log("Android Buld Version: ", android.os.Build.VERSION.SDK_INT)
            if (!hasPermission) {
                console.log("Permission is not granted yet");
                hasPermission = android.content.pm.PackageManager.PERMISSION_GRANTED ==
                android.support.v4.content.ContextCompat.checkSelfPermission(application.android.foregroundActivity, permissionConstant);
            }
            if(!hasPermission) {
                if(android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale(application.android.foregroundActivity, permissionConstant)){
                    resolve(-1);
                } else {
                    console.log("Requesting for permission from Android now");
                    android.support.v4.app.ActivityCompat.requestPermissions(
                        application.android.foregroundActivity,
                        [permissionConstant],
                        requestCode);
                }
            }
            resolve(hasPermission);
        });
    }
}