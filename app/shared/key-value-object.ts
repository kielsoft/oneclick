
interface IValueItem {
    ValueMember: any
    DisplayMember: any
}

export class ValueList {
    private _array: Array<IValueItem>;

    get length(): number { return this._array.length; }

    constructor(array: Array<IValueItem>) {
        this._array = array;
    }

    public getItem(index: number) { // Used for items source in list picker
        return this.getText(index);
    }

    public getText(index: number): string {
        if (index < 0 || index >= this._array.length) {
            return "";
        }

        return this._array[index].DisplayMember;
    }

    public getValue(index: number) {
        if (index < 0 || index >= this._array.length) {
            return null;
        }

        return this._array[index].ValueMember;
    }

    public getItemObjectAt(index: number): IValueItem {
        if (index < 0 || index >= this._array.length) {
            return null;
        }

        return this._array[index];
    }

    public getIndex(value: any): number {
        let loop: number;

        for (loop = 0; loop < this._array.length; loop++) {
            if (this.getValue(loop) == value) {
                return loop;
            }
        }

        return 0;
    }
}

interface ITelco {
    Id: string,
    Name: string,
    AirtimeBalanceCode: string,
    DataBalanceCode: string
}

interface ITelcoTransfer {
    Id: string,
    Name: string,
    TransferTemplate: string,
    ChangePasswordTemplate: string,
    DefaultPassword: string
}

class ObjectTemplate {
    public _array: Array<any>;

    get length(): number { return this._array.length; }

    public getItem(index: number) { // Used for items source in list picker
        return this.getText(index);
    }

    public getText(index: number): string {
        if (index < 0 || index >= this._array.length) {
            return "";
        }

        return this._array[index].Name;
    }

    public getValue(index: number) {
        if (index < 0 || index >= this._array.length) {
            return null;
        }

        return this._array[index].Id;
    }

    public getItemObjectAt(index: number): any {
        if (index < 0 || index >= this._array.length) {
            return null;
        }

        return this._array[index];
    }

    public getIndex(value: any): number {
        let loop: number;

        for (loop = 0; loop < this._array.length; loop++) {
            if (this.getValue(loop) == value) {
                return loop;
            }
        }

        return -1;
    }

}

export class Telco extends ObjectTemplate {
    public _array: Array<ITelco>;

    get length(): number { return this._array.length; }

    constructor(array: Array<ITelco>) {
        super();
        this._array = array;
    }

    public getItemObjectAt(index: number): ITelco {
        if (index < 0 || index >= this._array.length) {
            return null;
        }

        return this._array[index];
    }
}

export class TelcoTransfer extends ObjectTemplate {
    public _array: Array<ITelcoTransfer>;

    get length(): number { return this._array.length; }

    constructor(array: Array<ITelcoTransfer>) {
        super();
        this._array = array;
    }

    public getItemObjectAt(index: number): ITelcoTransfer {
        if (index < 0 || index >= this._array.length) {
            return null;
        }

        return this._array[index];
    }

}