import { Component, Input } from "@angular/core";

@Component({
    selector: "Fonticon",
    template: `<Label #innerLabel [class]="fullClass" [text]="faClass | fonticon" textWrap="true"></Label>`,
})
export class FonticonComponent {
    @Input() faClass: string;
    @Input() styleClass: string;

    public fullClass: string;

    ngOnInit() {
        this.fullClass = this.styleClass ? `fa ${this.styleClass}` : "fa";
        console.log("=====================================");
        console.log(this.faClass, this.fullClass);
    }
}