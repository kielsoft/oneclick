import 'reflect-metadata';
import { Component } from "@angular/core";
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { TNSFontIconService } from 'nativescript-ng2-fonticon';

@Component({
    selector: "my-app",
    template: `<page-router-outlet></page-router-outlet>`,
})
export class AppComponent {
    constructor(private fonticon: TNSFontIconService) {
        // ^ IMPORTANT to cause Angular's DI system to instantiate the service!
    }

}