// this import should be first in order to load some required settings (like globals and reflect-metadata)
import 'reflect-metadata';
import { platformNativeScriptDynamic, NativeScriptModule } from "nativescript-angular/platform";
import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { TNSFontIconService } from 'nativescript-ng2-fonticon';

import { registerElement } from "nativescript-angular/element-registry";
registerElement("DropDown", () => require("nativescript-drop-down/drop-down").DropDown);

import { setStatusBarColors } from "./shared/status-bar-util";
import { ComponentsModule, routes } from "./components.module";
import { AppComponent } from "./app.component";

setStatusBarColors();

@NgModule({
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    imports: [
        ComponentsModule,
        NativeScriptRouterModule.forRoot(routes),
        
    ]
})
class AppComponentModule { }

platformNativeScriptDynamic().bootstrapModule(AppComponentModule);